Spotify Search app
===
Esta applicación se integra con la API de Spotify para realizar busquedas en la biblioteca y provee el play button de Spotify para acceder a las caciones seleccionadas.

Dependencias
---
+ [Python](https://www.python.org/downloads/) --> Probado en la versión (3.6.1)
+ [Django](https://www.djangoproject.com/)
+ [spotipy](https://spotipy.readthedocs.io/en/latest/)

Para instalar las dependencias usar `pip`. Está disponible por defecto en Python 3.6.1
```
pip install django
pip install spotipy
```

Ejecutando la Aplicación
---
Para acceder a la API de spotify es necesario registrar la [aplicación](https://developer.spotify.com/my-applications/#!/applications) y generar las claves de conexión. Estas claves las va a necesitar spotipy para poder conectarse y consumir el servicio de Spotify así que es necesario configurarlas como variables de entorno con los nombres `SPOTIPY_CLIENT_ID` y `SPOTIPY_CLIENT_SECRET`

Después de configurar las variables de entorno ya solo falta iniciar el servidor de la aplicación de la siguiente manera: `py manage.py runserver 0.0.0.0:<port>`.

Tambien hay disponible un script batch que se encarga de configurar las variables de entorno en un valor por defecto e inicia el servidor en el puerto 8080 para usarlo solo ejecute el archivo `startapp.bat` que se encuentra en la raiz del repositorio.

Ahora en el navegador web vaya a la dirección [localhost:8080](http://localhost:8080).