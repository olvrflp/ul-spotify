
var sendRequest = function(callbak, url, params) {
    $.get({
        url: url,
        data: params
    }).done(function(response){
        callbak(response);
        waitingDialog.hide();
    });
};

var updateContentCallback = function(htmlContent){
    $('#content').html(htmlContent);
};

var search = function(term){
    waitingDialog.show('Buscando ...');
    sendRequest(updateContentCallback, '/buscar', 'trackName=' + term);
};

var openSelectedResult = function(uri){
    waitingDialog.show('Abriendo ...');
    sendRequest(updateContentCallback, '/play', 'track=' + uri);
    return true;
};

var submitSearchForm = function(event){
    event.preventDefault();
    term = $('#searchTerm').val();
    if (term !== undefined && term.trim() !== '')
    {
        search(term);
    }
};

$(document).ready(function(){
    $('#searchForm').submit(submitSearchForm);
});