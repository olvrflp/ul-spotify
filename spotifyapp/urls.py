from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^buscar/$', views.buscar, name='buscar'),
    url(r'^$', views.index, name='index'),
    url(r'^play/$', views.play, name='play'),
]
