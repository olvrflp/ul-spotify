from django.shortcuts import render
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

spotify = None
clientCredentials = SpotifyClientCredentials()

def buscar(request):
    result = spotify.search(q=request.GET['trackName'])
    context = {
        "resultado" : result['tracks']['items'],
    }
    return render(request, 'buscar.html', context)

def index(request):
    global spotify
    spotify = spotipy.Spotify(client_credentials_manager=clientCredentials)
    context = {}
    return render(request, 'spotifysearch.html', context)

def play(request):
    #track = spotify.track(request.GET['track'])
    context = {
        "track" : request.GET['track']
    }
    return render(request, 'play.html', context)
